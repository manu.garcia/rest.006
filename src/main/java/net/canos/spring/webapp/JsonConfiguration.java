package net.canos.spring.webapp;

import java.util.List;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;

@Configuration
@EnableWebMvc
public class JsonConfiguration extends WebMvcConfigurerAdapter {
	
	
	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters){
		Jackson2ObjectMapperBuilder mapperbuilder = new Jackson2ObjectMapperBuilder();
		mapperbuilder = mapperbuilder.propertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
		mapperbuilder = mapperbuilder.serializationInclusion(Include.NON_EMPTY);
		
		MappingJackson2HttpMessageConverter map = new MappingJackson2HttpMessageConverter(mapperbuilder.build());
		converters.add(map);
		
	}
}
